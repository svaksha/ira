+ [Proposal](#proposal)
+ [Abstract-Short](#abstract-short) 
+ [Abstract-Detailed](#abstract-detailed)
    + [Objectives](#objectives)
    + [NoteBook-1](#notebook-1)
    + [NoteBook-2](#notebook-2)
    + [NoteBook-3](#notebook-3)
    + [NoteBook-4](#notebook-4)
    + [NoteBook-5](#notebook-5)
+ [Biography](#biography)
+ [Additional Requirements](#additional-requirements)
+ [Additional Speakers](#additional-speakers)
+ [Feedback](#feedback)

----

# Proposal
#### Event :: Conference (Talk / Workshop)
#### Title :: ?
+ __Author(s)__:: SVAKSHA
+ __Contact__  :: <svaksha-AT-gmail-DOT-com>
+ __Date__     :: YYYY-MM-DD
+ __Category__ :: Core / Lib / Web/ Science / ...
+ __Duration__ :: 30/45 min
+ __Level__    :: Novice / Intermediate / Expert

----

# Abstract-Short 
{{*If the talk is accepted this will be made public and printed in the program. Should be one paragraph, maximum 400 characters.*}}


----

# Abstract-Detailed
{{*Detailed description. Will be made public if your talk is accepted.*}}

### Objectives
{{*What will attendees get out of your talk? When they leave the room, what will they know that they didn't know before?*}}

### NoteBook-1
### NoteBook-2
### NoteBook-3
### NoteBook-4
### NoteBook-5

----

# Biography
{{*Anything else you'd like the program committee to know when making their selection: your past speaking experience, open source community experience, etc.*}}

[SVAKSHA](http://svaksha.com/pages/Bio) is a Foss contributor who has spoken at coding camps and technical conferences like [Pycon-CA](http://2012.pycon.ca/talk/29). Recently, her [Bootstrapping your Foss journey with Julia](http://nbviewer.ipython.org/github/svaksha/ira/blob/master/2015-07-fsmkcamp/00_index.ipynb) talk at FSMK-Camp was very well received and rated as "worth it / [higher standard](http://harikavreddy.blogspot.in/2015/07/i-always-wanted-my-holidays-to-be.html)" by the student attendees. 

Here is the full list of her [publications and technical talks](http://svaksha.com/pages/Publications) presented at conferences. The Jupyter notebooks and slide materials used in her talks and presentations (including conference proposals) have been released under the AGPL/CC licenses in the [IRA](http://svaksha.github.io/ira) repo on github.

----

# Additional Requirements

----

# Additional Speakers

----

# Feedback
{{*Organizers feedback*}}

----

