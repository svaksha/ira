+ [Proposal](#proposal)
+ [Abstract-Short](#abstract-short) 
+ [Abstract-Detailed](#abstract-detailed)
    + [Objectives](#objectives)
    + [NoteBook-1](#notebook-1)
    + [NoteBook-2](#notebook-2)
    + [NoteBook-3](#notebook-3)
    + [NoteBook-4](#notebook-4)
    + [NoteBook-5](#notebook-5)
+ [Biography](#biography)
+ [Additional Requirements](#additional-requirements)
+ [Additional Speakers](#additional-speakers)
+ [Feedback](#feedback)

----

# Proposal 

#### Event :: Aceconf-2016

#### Title :: `import julia`
+ __Author(s)__:: SVAKSHA
+ __Contact__  :: svaksha-AT-gmail-DOT-com
+ __Date__     :: 2015-10-08
+ __Category__ :: Managing complexity, Software craftsmanship
+ __Duration__ :: 30 min (or 45 min depending on the organizers feeback)
+ __Level__    :: Novice.

----

# Abstract-Short 
{{*If your talk is accepted this will be made public and printed in the program. Should be one paragraph, maximum 400 characters.*}}

Julia is a new scientific computing language with dynamic features that improve the scientific and numerical computing experience. In this talk session, I will introduce the language basics and talk about the advantages of Julia, how the programmer (or researcher) can leverage the Julia language into their processing tasks, the language features, its uses in the research world and look at code samples.

----

# Abstract-Detailed
{{*Detailed description - public, if talk is accepted.*}}


### Objectives
{{*What will attendees get out of your talk? When they leave the room, what will they know that they didn't know before?*}}

The attendees (programmers and users) will ...
+ get introduced to the Julia programming language,
+ have an overview of different libraries currently available,
+ developers can try the simple syntax and the libs available,
+ leave the talk with a good understanding of scientific programming with Julia,
+ understand how they can incorporate Julia code within their current codebase,
+ want to use Julia for analysing large distributed datasets. 

My talk will use Jupyter notebooks for the presentation and being released under the Foss/CC licence, these can be downloaded, edited and re-used under the same licence and CR terms by anyone.

### NoteBook-1
### NoteBook-2
### NoteBook-3
### NoteBook-4
### NoteBook-5

----

# Biography
{{*Anything else you'd like the program committee to know when making their selection: your past speaking experience, open source community experience, etc.*}}

[SVAKSHA](http://svaksha.com/pages/Bio) is a Foss contributor who has spoken at coding camps and technical conferences like [Pycon-CA](http://2012.pycon.ca/talk/29). Recently, her [Bootstrapping your Foss journey with Julia](http://nbviewer.ipython.org/github/svaksha/ira/blob/master/2015-07-fsmkcamp/00_index.ipynb) talk at FSMK-Camp was very well received and rated as "worth it / [higher standard](http://harikavreddy.blogspot.in/2015/07/i-always-wanted-my-holidays-to-be.html)" by the student attendees. 

Here is the full list of her [publications and technical talks](http://svaksha.com/pages/Publications) presented at conferences. The Jupyter notebooks and slide materials used in her talks and presentations (including conference proposals) have been released under the AGPL/CC licenses in the [IRA](http://svaksha.github.io/ira) repo on github.

----

# Additional Requirements

None.

----

# Additional Speakers

None.

----

# Feedback
{{*Organizers feedback*}}

----

